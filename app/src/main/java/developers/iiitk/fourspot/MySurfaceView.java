package developers.iiitk.fourspot;

import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.util.Log;
import android.view.SurfaceView;

/**
 * Created by Joey on 4/10/2016.
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.RatingBar;

public class MySurfaceView extends SurfaceView {
static int orientation;
     float[] mGravity;
     float[] mMagnetic;
    private SurfaceHolder surfaceHolder;
    private Bitmap bmpIcon;
    private MyThread myThread;

    float xPos = 0;
    float yPos = 0;
    float deltaX = 15;
    float deltaY = 15;
    float iconWidth;
    float iconHeight;
    float paddleX=0;
    float paddleWidth=10;
    float paddleHeight=20;
    float position_user=0;
    float position_comp=0;
    boolean exit=false;
    public MySurfaceView(Context context) {
        super(context);
        init();
    }

    public MySurfaceView(Context context,
                         AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MySurfaceView(Context context,
                         AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){

        myThread = new MyThread(this);

        surfaceHolder = getHolder();
        bmpIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ball);

        iconWidth = bmpIcon.getWidth();
        iconHeight = bmpIcon.getHeight();

        surfaceHolder.addCallback(new SurfaceHolder.Callback(){

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                myThread.setRunning(true);
                myThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder,
                                       int format, int width, int height) {
                // TODO Auto-generated method stub

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                myThread.setRunning(false);
                while (retry) {
                    try {
                        myThread.join();
                        retry = false;
                    } catch (InterruptedException e) {
                    }
                }
            }});
       }

    protected void drawSomething(Canvas canvas)
    {

  if(orientation>0 && position_user<75){
      position_user+=5;
  }
        if(orientation<0 && position_user>-75){
      position_user-=5;
  }
       if(yPos<getHeight()*3/4) {
           if (deltaX > 0 && position_comp < 75) {
               position_user += 5;
           }
           if (deltaX < 0 && position_comp > -75) {
               position_user -= 5;
           }
       }
        //position=orientation*2;
        float width = getWidth();
        float height = getHeight();
        Paint paint = new Paint();
        paddleHeight=20;
        paddleWidth=width/5;

        float factor=(getWidth()-2*paddleWidth)/100;
        float start_user=getWidth()/2-paddleWidth/2+position_user*factor;
        float start_comp=getWidth()/2-paddleWidth/2+position_comp*factor;

        float end_user=getWidth()/2+paddleWidth/2+position_user*factor;
        float end_comp=getWidth()/2+paddleWidth/2+position_comp*factor;


       if((getHeight()-iconHeight-paddleHeight)-yPos<=iconWidth
               &&(xPos>start_user-iconWidth/3&&xPos<end_user+iconWidth/3))Home.v.vibrate(50);
       else if(yPos>getHeight() - iconHeight){


           if(Home.getRating()==1){
              exit=true ;
               myThread.setRunning(false);
           }
 else
      Home.setRating(Home.getRating()-1);
           canvas.drawColor(Color.RED);
           Home.v.vibrate(2000);
       }
        else    canvas.drawColor(Color.WHITE);



        xPos += deltaX;
        if(deltaX > 0){
            if(xPos >= getWidth() - iconWidth){
                deltaX *= -1;
            }
        }else{
            if(xPos <= 0){
                deltaX *= -1;
            }
        }

        yPos += deltaY;
        if(deltaY > 0  ){
            if(yPos >= getHeight() - iconHeight){
                deltaY *= -1;
            }
        }else{
            if(yPos <= 0){
                deltaY *= -1;
            }
        }


        canvas.drawBitmap(bmpIcon,
                xPos, yPos, null);



        paint.setColor(Color.RED);
        paint.setStrokeWidth(3);

      //  System.out.print(position);
        canvas.drawRect( start_user, height-paddleHeight, end_user, height, paint);
        canvas.drawRect( start_comp, 0, end_comp, paddleHeight, paint);


    }

}